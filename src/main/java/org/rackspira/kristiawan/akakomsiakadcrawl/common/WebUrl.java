package org.rackspira.kristiawan.akakomsiakadcrawl.common;

/**
 * Created by kal on 6/25/2017.
 */
public class WebUrl {

    public static String BASE_URL = "https://siakad.akakom.ac.id";
    public static final String LOGIN = BASE_URL + "/index.php?pModule=zdKbnKU=&pSub=zdKbnKU=&pAct=0dWjppyl";
    public static final String PROFILE = BASE_URL + "/index.php?pModule=1taZpQ==&pSub=0dWjmaCemQ==&pAct=18yZqg==";
    public static final String KARTU_HASIL_STUDI = BASE_URL + "/index.php?pModule=wsaVl5yfncmQqMqpoaal&pSub=wsaVl5yfncmQqMqpoaal&pAct=18yZqg==";
    public static final String KARTU_RENCANA_STUDI = BASE_URL + "/index.php?pModule=wsaVl5yfncmQptGaoA==&pSub=wsaVl5yfncmQptGaoA==&pAct=18yZqg==";
    public static final String PRESENSI = BASE_URL + "/index.php?pModule=wsaVl5yfncmQqMqpoaal&pSub=wsaVl5yfncmQpteepZmf16I=&pAct=18yZqg==";
    public static final String TRANSKIP_NILAI = BASE_URL + "/index.php?pModule=wsaVl5yfncmQqMqpoaal&pSub=wsaVl5yfncmQqteaoKeU1qKppA==&pAct=18yZqg==";
    public static final String LOGOUT = BASE_URL + "/index.php?pModule=zdKbnKU=&pSub=zdKboqym&pAct=0dWjppyl";

}
