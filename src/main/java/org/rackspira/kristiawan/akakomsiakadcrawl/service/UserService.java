package org.rackspira.kristiawan.akakomsiakadcrawl.service;

import com.google.gson.Gson;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.rackspira.kristiawan.akakomsiakadcrawl.App;
import org.rackspira.kristiawan.akakomsiakadcrawl.common.Constants;
import org.rackspira.kristiawan.akakomsiakadcrawl.common.WebUrl;
import org.rackspira.kristiawan.akakomsiakadcrawl.model.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.Map;
import java.util.logging.Logger;

import org.jsoup.Connection.Response;

/**
 * Created by kal on 6/25/2017.
 */
@Service
@Transactional
public class UserService extends AppService {

    public User login(String nim, String password) {
        try {
            Response response = Jsoup.connect(WebUrl.LOGIN)
                    .method(Connection.Method.POST)
                    .data("username", nim)
                    .data("password", password)
                    .timeout(12000)
                    .execute();

            Elements a = response.parse().select("#user-info");
            if (a.isEmpty()) {
                return null;
            } else {
                Map<String, String> cookies = response.cookies();
                User user = new User();
                user.setNama(a.select("h3").text());
                user.setNim(a.select("h4").eq(0).text());
                user.setJurusan(a.select("h4").eq(1).text());
                user.setPassword(password);
                user.setSession(cookies.get(Constants.SESSION));
                return user;
            }
        } catch (IOException e) {
            App.getLogger(this).debug(e.getMessage());
        }

        return null;
    }

    public String logout(String session) {
        try {
            Document doc = jsoupGet(WebUrl.LOGOUT, session);
            Elements elementsLogin = doc.select("#loginBox-body");
            if (!elementsLogin.isEmpty()) {
                return "SUKSES";
            }
        } catch (IOException e) {
            App.getLogger(this).debug(e.getMessage());
        }

        return null;
    }
}
