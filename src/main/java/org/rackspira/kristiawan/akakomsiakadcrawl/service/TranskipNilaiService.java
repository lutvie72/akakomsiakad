package org.rackspira.kristiawan.akakomsiakadcrawl.service;


import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.rackspira.kristiawan.akakomsiakadcrawl.App;
import org.rackspira.kristiawan.akakomsiakadcrawl.common.WebUrl;
import org.rackspira.kristiawan.akakomsiakadcrawl.model.MataKuliah;
import org.rackspira.kristiawan.akakomsiakadcrawl.model.TranskipNilai;
import org.rackspira.kristiawan.akakomsiakadcrawl.model.TranskipNilaiDetail;
import org.rackspira.kristiawan.akakomsiakadcrawl.model.User;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kal on 6/28/2017.
 */
@Service
public class TranskipNilaiService extends AppService implements BaseService<TranskipNilai> {

    public TranskipNilai find(String session) {
        try {
            Document doc = jsoupGet(WebUrl.TRANSKIP_NILAI, session);
            Elements elementsUser = doc.select(".table-list td");
            Elements elementsNilai = doc.select(".table-common tr");

            if (!elementsNilai.isEmpty()) {
                Elements elementsPrestasi = doc.select("#content table").get(2).select("td");
                User user = new User();
                user.setNama(elementsUser.get(0).text());
                user.setNim(elementsUser.get(1).text());
                user.setJurusan(elementsUser.get(2).text());

                List<TranskipNilaiDetail> transkipNilaiDetails = new ArrayList<TranskipNilaiDetail>();
                for (int i = 1; i < elementsNilai.size(); i++) {
                    Elements elementsTd = elementsNilai.get(i).select("td");

                    TranskipNilaiDetail transkipNilaiDetail = new TranskipNilaiDetail();
                    transkipNilaiDetail.setNo(Integer.valueOf(elementsTd.get(0).text()));
                    transkipNilaiDetail.setSemester(elementsTd.get(1).text());
                    transkipNilaiDetail.setMataKuliah(new MataKuliah(elementsTd.get(2).text(), elementsTd.get(3).text(), Integer.valueOf(elementsTd.get(4).text())));
                    transkipNilaiDetail.setNilai(elementsTd.get(5).text());
                    transkipNilaiDetails.add(transkipNilaiDetail);
                }

                TranskipNilai transkipNilai = new TranskipNilai();
                transkipNilai.setJudul(doc.select("#content h2").text());
                transkipNilai.setUser(user);
                transkipNilai.setTranskipNilaiDetails(transkipNilaiDetails);
                transkipNilai.setJumlahSksDiambil(elementsPrestasi.get(1).text().substring(2));
                transkipNilai.setJumlahMatakuliahDiambil(elementsPrestasi.get(3).text().substring(2));
                transkipNilai.setIpKomulatif(elementsPrestasi.get(5).text().substring(2));
                return transkipNilai;
            }
        } catch (IOException e) {
            App.getLogger(this).debug(e.getMessage());
        }
        return null;
    }
}
