package org.rackspira.kristiawan.akakomsiakadcrawl.service;

/**
 * Created by kal on 6/25/2017.
 */
public interface BaseService <T> {

    T find(String session);
}
