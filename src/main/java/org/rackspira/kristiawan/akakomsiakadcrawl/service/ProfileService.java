package org.rackspira.kristiawan.akakomsiakadcrawl.service;

import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.rackspira.kristiawan.akakomsiakadcrawl.App;
import org.rackspira.kristiawan.akakomsiakadcrawl.common.WebUrl;
import org.rackspira.kristiawan.akakomsiakadcrawl.model.Profile;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * Created by kal on 8/27/2017.
 */
@Service
public class ProfileService extends AppService{

    public Profile find(String session){
        try {
            Document doc = jsoupGet(WebUrl.PROFILE, session);
            Elements elementsProfile = doc.select(".table-list td");
            if (!elementsProfile.isEmpty()){
                Profile profile = new Profile();
                profile.setNim(elementsProfile.get(0).text());
                profile.setNama(elementsProfile.get(1).text());
                profile.setAlamat(elementsProfile.get(2).text());
                profile.setTtl(elementsProfile.get(3).text());
                profile.setAgama(elementsProfile.get(4).text());
                profile.setJenisKelamin(elementsProfile.get(5).text());
                profile.setAsalSLTA(elementsProfile.get(6).text());
                profile.setTglTerdaftar(elementsProfile.get(7).text());
                profile.setNamaOrtuAyahIbu(elementsProfile.get(8).text());
                profile.setAlamatOrtu(elementsProfile.get(9).text());
                profile.setWargaNegara(elementsProfile.get(10).text());
                profile.setStatus(elementsProfile.get(11).text());
                profile.setNoTelpHp(elementsProfile.get(12).text());
                return profile;
            }
        } catch (IOException e) {
            App.getLogger(this).debug(e.getMessage());
        }

        return null;
    }
}
