package org.rackspira.kristiawan.akakomsiakadcrawl.service;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.rackspira.kristiawan.akakomsiakadcrawl.App;
import org.rackspira.kristiawan.akakomsiakadcrawl.common.Constants;
import org.rackspira.kristiawan.akakomsiakadcrawl.common.WebUrl;
import org.rackspira.kristiawan.akakomsiakadcrawl.model.KartuRencanaStudi;
import org.rackspira.kristiawan.akakomsiakadcrawl.model.KartuRencanaStudiJadwal;
import org.rackspira.kristiawan.akakomsiakadcrawl.model.MataKuliah;
import org.rackspira.kristiawan.akakomsiakadcrawl.model.User;
import org.springframework.stereotype.Service;

import javax.print.Doc;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kal on 6/25/2017.
 */
@Service
public class KartuRencanaStudiService extends AppService implements BaseService<KartuRencanaStudi> {

    public KartuRencanaStudi find(String session) {
        try {
            Document doc = Jsoup.connect(WebUrl.KARTU_RENCANA_STUDI)
                    .cookie(Constants.SESSION, session)
                    .get();

            Elements button = doc.select(".input-disclaimer");
            if (button.isEmpty()) {
                return null;
            }

            String url = button.last().attr("onClick").split("'")[1];

            if (url != null)
                doc = Jsoup.connect(WebUrl.BASE_URL + "/" + url)
                        .cookie(Constants.SESSION, session)
                        .get();

            Elements main = doc.select(".nobreak");
            if (!main.isEmpty()) {
                Elements tabels = main.select("table");
                Elements tr = tabels.get(0).select("tbody").select("tr");

                KartuRencanaStudi kartuRencanaStudi = new KartuRencanaStudi();
                kartuRencanaStudi.setJudul(main.select("h2").text());
                kartuRencanaStudi.setSemester(main.select("h3").text().substring(11));

                User user = new User();
                user.setNama(tr.get(0).select("td").get(2).text());
                user.setNim(tr.get(1).select("td").get(2).text());
                user.setJurusan(tr.get(2).select("td").get(2).text());
                user.setDosenPA(tr.get(3).select("td").get(2).text());
                kartuRencanaStudi.setUser(user);

                tr = tabels.get(1).select("tbody").select("tr");
                List<KartuRencanaStudiJadwal> kartuRencanaStudiJadwals = new ArrayList<KartuRencanaStudiJadwal>();
                for (int i = 0; i < tr.size(); i++) {
                    Elements elementsTd = tr.get(i).select("td");
                    if (i != 0 && i != 1 && i != (tr.size() - 1)) {
                        KartuRencanaStudiJadwal jadwal = new KartuRencanaStudiJadwal();
                        jadwal.setNo(Integer.valueOf(elementsTd.get(0).text()));
                        jadwal.setKelas(Integer.valueOf(elementsTd.get(1).text()));
                        jadwal.setMataKuliah(new MataKuliah(elementsTd.get(2).text(), elementsTd.get(3).text(), Integer.valueOf(elementsTd.get(4).text())));
                        jadwal.setKe(Integer.valueOf(elementsTd.get(5).text()));
                        jadwal.setRuang(elementsTd.get(6).text());
                        for (int h = 0; h < 6; h++) {
                            Element element = elementsTd.get(7 + h);
                            if (!element.text().equals("")) {
                                jadwal.setHari(Constants.HARI[h]);
                                jadwal.setJam(element.text());
                            }
                        }

                        kartuRencanaStudiJadwals.add(jadwal);
                    }

                    if (i == (tr.size() - 1)) {
                        kartuRencanaStudi.setJumlahKredit(Integer.valueOf(elementsTd.get(1).text()));
                    }
                }

                kartuRencanaStudi.setKartuRencanaStudiJadwals(kartuRencanaStudiJadwals);

                tr = main.select(".tabel-info").select("tr");
                kartuRencanaStudi.setIpSemesterLalu(tr.get(0).select("td").get(1).text().substring(2));
                kartuRencanaStudi.setMaksimalSks(tr.get(1).select("td").get(1).text().substring(2));
                return kartuRencanaStudi;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
