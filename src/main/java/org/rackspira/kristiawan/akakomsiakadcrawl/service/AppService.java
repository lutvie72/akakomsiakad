package org.rackspira.kristiawan.akakomsiakadcrawl.service;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.rackspira.kristiawan.akakomsiakadcrawl.common.Constants;

import java.io.IOException;
import java.util.Map;

/**
 * Created by kal on 6/26/2017.
 */
public class AppService {

    Document jsoupGet(String url, String cookie) throws IOException {
        return Jsoup.connect(url)
                .cookie(Constants.SESSION, cookie)
                .timeout(12000)
                .get();
    }

    Document jsoupPost(String url, String cookie, Map<String, String> params) throws IOException {
        return Jsoup.connect(url)
                .cookie(Constants.SESSION, cookie)
                .data(params)
                .timeout(12000)
                .post();
    }
}
