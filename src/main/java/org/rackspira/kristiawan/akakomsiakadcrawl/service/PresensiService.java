package org.rackspira.kristiawan.akakomsiakadcrawl.service;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.rackspira.kristiawan.akakomsiakadcrawl.App;
import org.rackspira.kristiawan.akakomsiakadcrawl.common.WebUrl;
import org.rackspira.kristiawan.akakomsiakadcrawl.model.*;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by kal on 6/26/2017.
 */
@Service
public class PresensiService extends AppService {

    public List<Semester> findSemester(String session) {
        List<Semester> semesters = new ArrayList<Semester>();
        try {
            Document doc = jsoupGet(WebUrl.PRESENSI, session);
            Elements elements = doc.select("option");
            if (!elements.isEmpty()){
                for (Element element : elements) {
                    semesters.add(new Semester(element.val(), element.text()));
                }

                return semesters;
            }
        } catch (IOException e) {
            App.getLogger(this).debug(e.getMessage());
        }

        return null;
    }

    public Presensi find(String session, Semester semester) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("lstSemester", semester.getKode());
        params.put("btnLihat", "Lihat");

        try {
            Document doc = jsoupPost(WebUrl.PRESENSI, session, params);
            Elements elements = doc.select(".table-list td");
            if (!elements.isEmpty()) {
                Presensi presensi = new Presensi();

                User user = new User();
                user.setNama(elements.get(0).text());
                user.setNim(elements.get(1).text());
                user.setJurusan(elements.get(2).text());
                presensi.setUser(user);
                presensi.setSemester(new Semester(null, elements.get(3).text()));

                elements = doc.select(".table-common tr");
                List<PresensiDetail> presensiDetails = new ArrayList<PresensiDetail>();
                for (int i = 1; i < elements.size(); i++) {
                    Elements elementsTd = elements.get(i).select("td");

                    PresensiDetail presensiDetail = new PresensiDetail();
                    presensiDetail.setNo(Integer.valueOf(elementsTd.get(0).text()));
                    presensiDetail.setMataKuliah(new MataKuliah(elementsTd.get(1).text(), elementsTd.get(2).text()));
                    presensiDetail.setKelas(elementsTd.get(3).text());
                    presensiDetail.setJumlahKuliah(elementsTd.get(4).text());
                    presensiDetail.setHadir(elementsTd.get(5).text());
                    presensiDetail.setIjin(elementsTd.get(6).text());
                    presensiDetail.setSakit(elementsTd.get(7).text());
                    presensiDetail.setAlpa(elementsTd.get(8).text());
                    presensiDetail.setHadirPersentase(elementsTd.get(9).text());
                    presensiDetails.add(presensiDetail);
                }

                presensi.setPresensiDetails(presensiDetails);
                return presensi;
            }
        } catch (IOException e) {
            App.getLogger(this).debug(e.getMessage());
        }

        return null;
    }
}
