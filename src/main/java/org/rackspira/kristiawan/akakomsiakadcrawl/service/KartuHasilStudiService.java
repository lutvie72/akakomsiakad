package org.rackspira.kristiawan.akakomsiakadcrawl.service;

import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.rackspira.kristiawan.akakomsiakadcrawl.App;
import org.rackspira.kristiawan.akakomsiakadcrawl.common.WebUrl;
import org.rackspira.kristiawan.akakomsiakadcrawl.model.*;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by kal on 6/28/2017.
 */
@Service
public class KartuHasilStudiService extends AppService {

    public KartuHasilStudi find(String session, Semester semester) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("lstSemester", semester.getKode());
        params.put("btnLihat", "Lihat");

        try {
            Document doc = jsoupPost(WebUrl.KARTU_HASIL_STUDI, session, params);
            Elements elementsUser = doc.select(".table-list td");
            Elements elementsNilai = doc.select(".table-common tr");

            if (!elementsNilai.isEmpty()) {
                Elements elementsPrestasi = doc.select("#content table").get(3).select("td");

                User user = new User();
                user.setNama(elementsUser.get(0).text());
                user.setNim(elementsUser.get(1).text());
                user.setJurusan(elementsUser.get(2).text());

                List<KartuHasilStudiDetail> kartuHasilStudiDetails = new ArrayList<KartuHasilStudiDetail>();
                for (int i = 1; i < elementsNilai.size(); i++) {
                    Elements elementsTd = elementsNilai.get(i).select("td");

                    KartuHasilStudiDetail kartuHasilStudiDetail = new KartuHasilStudiDetail();
                    kartuHasilStudiDetail.setNo(Integer.valueOf(elementsTd.get(0).text()));
                    kartuHasilStudiDetail.setMataKuliah(new MataKuliah(elementsTd.get(1).text(), elementsTd.get(2).text(), Integer.valueOf(elementsTd.get(5).text())));
                    kartuHasilStudiDetail.setKelas(elementsTd.get(3).text());
                    kartuHasilStudiDetail.setWajibAtauPilihan(elementsTd.get(4).text());
                    kartuHasilStudiDetail.setNilai(elementsTd.get(6).text());
                    kartuHasilStudiDetails.add(kartuHasilStudiDetail);
                }

                KartuHasilStudi kartuHasilStudi = new KartuHasilStudi();
                kartuHasilStudi.setJudul(doc.select("#content h2").text());
                kartuHasilStudi.setUser(user);
                kartuHasilStudi.setSemester(elementsUser.get(3).text());
                kartuHasilStudi.setKartuHasilStudiDetails(kartuHasilStudiDetails);
                kartuHasilStudi.setJumlahSksDiambil(elementsPrestasi.get(1).text().substring(2));
                kartuHasilStudi.setJumlahMatakuliahDiambil(elementsPrestasi.get(3).text().substring(2));
                kartuHasilStudi.setIpSemester(elementsPrestasi.get(5).text().substring(2));
                return kartuHasilStudi;
            }
        } catch (IOException e) {
            App.getLogger(this).debug(e.getMessage());
        }

        return null;
    }
}
