package org.rackspira.kristiawan.akakomsiakadcrawl.controller;

import org.rackspira.kristiawan.akakomsiakadcrawl.model.TranskipNilai;
import org.rackspira.kristiawan.akakomsiakadcrawl.service.TranskipNilaiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * Created by kal on 6/28/2017.
 */
@RestController
public class TranskipNilaiController extends BaseController {

    @Autowired
    private TranskipNilaiService service;

    @RequestMapping(value = "/transkip-nilai", method = RequestMethod.GET)
    public Map<String, Object> find(@RequestParam String session){
        TranskipNilai result = service.find(session);
        if (result != null){
            return convertModel(result, HttpStatus.OK);
        }

        return convertModel(null, HttpStatus.NOT_FOUND);
    }
}
