package org.rackspira.kristiawan.akakomsiakadcrawl.controller;

import org.rackspira.kristiawan.akakomsiakadcrawl.model.Presensi;
import org.rackspira.kristiawan.akakomsiakadcrawl.model.Semester;
import org.rackspira.kristiawan.akakomsiakadcrawl.service.PresensiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * Created by kal on 6/26/2017.
 */
@RestController
public class PresensiController extends BaseController{

    @Autowired
    PresensiService service;

    @RequestMapping(value = "/semesters", method = RequestMethod.GET)
    public Map<String, Object> findSemester(@RequestParam String session){
        List<Semester> result = service.findSemester(session);
        if (result != null){
            return convertModel(result, HttpStatus.OK);
        }

        return convertModel(null, HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/presensi", method = RequestMethod.POST)
    public Map<String, Object> find(@RequestParam String session,
                                    @RequestBody Semester semester){
        Presensi result = service.find(session, semester);
        if (result != null){
            return convertModel(result, HttpStatus.OK);
        }

        return convertModel(null, HttpStatus.NOT_FOUND);
    }
}
