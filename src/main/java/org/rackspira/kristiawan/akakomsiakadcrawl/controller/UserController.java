package org.rackspira.kristiawan.akakomsiakadcrawl.controller;

import org.rackspira.kristiawan.akakomsiakadcrawl.model.Profile;
import org.rackspira.kristiawan.akakomsiakadcrawl.model.User;
import org.rackspira.kristiawan.akakomsiakadcrawl.service.ProfileService;
import org.rackspira.kristiawan.akakomsiakadcrawl.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * Created by kal on 6/25/2017.
 */
@RestController
public class UserController extends BaseController {

    @Autowired
    private UserService service;
    @Autowired
    private ProfileService profileService;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public Map<String, Object> login(@RequestParam String username,
                                     @RequestParam String password) {

        User result = service.login(username, password);
        if (result == null) {
            return convertModel(null, HttpStatus.NOT_FOUND);
        }

        return convertModel(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public Map<String, Object> logout(@RequestParam String session) {
        String result = service.logout(session);
        if (result != null) {
            return convertModel(result, HttpStatus.OK);
        }

        return convertModel(null, HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    public Map<String, Object> findProfile(@RequestParam String session) {
        Profile result = profileService.find(session);
        if (result != null) {
            return convertModel(result, HttpStatus.OK);
        }

        return convertModel(null, HttpStatus.NOT_FOUND);
    }
}
