package org.rackspira.kristiawan.akakomsiakadcrawl.controller;

import org.rackspira.kristiawan.akakomsiakadcrawl.model.KartuHasilStudi;
import org.rackspira.kristiawan.akakomsiakadcrawl.model.Semester;
import org.rackspira.kristiawan.akakomsiakadcrawl.service.KartuHasilStudiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * Created by kal on 6/28/2017.
 */
@RestController
public class KartuHasilStudiController extends BaseController {

    @Autowired
    private KartuHasilStudiService service;

    @RequestMapping(value = "/kartu-hasil-studi", method = RequestMethod.POST)
    public Map<String, Object> find(@RequestParam String session,
                                    @RequestBody Semester semester) {
        KartuHasilStudi result = service.find(session, semester);
        if (result != null) {
            return convertModel(result, HttpStatus.OK);
        }

        return convertModel(null, HttpStatus.NOT_FOUND);
    }
}
