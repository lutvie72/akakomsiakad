package org.rackspira.kristiawan.akakomsiakadcrawl.controller;

import org.rackspira.kristiawan.akakomsiakadcrawl.model.KartuRencanaStudi;
import org.rackspira.kristiawan.akakomsiakadcrawl.service.KartuRencanaStudiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * Created by kal on 6/26/2017.
 */

@RestController
public class KartuRencanaStudiController extends BaseController{

    @Autowired
    KartuRencanaStudiService service;

    @RequestMapping(value = "/kartu-rencana-studi", method = RequestMethod.GET)
    public Map<String, Object> find(@RequestParam String session){
        KartuRencanaStudi result = service.find(session);
        if (result != null){
            return convertModel(result, HttpStatus.OK);
        }

        return convertModel(service.find(session), HttpStatus.NOT_FOUND);
    }
}
