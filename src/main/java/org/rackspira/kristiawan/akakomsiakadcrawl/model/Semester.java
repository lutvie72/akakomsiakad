package org.rackspira.kristiawan.akakomsiakadcrawl.model;

/**
 * Created by kal on 6/26/2017.
 */
public class Semester {

    private String kode;
    private String nama;

    public Semester() {
    }

    public Semester(String kode) {
        this.kode = kode;
    }

    public Semester(String kode, String nama) {
        this.kode = kode;
        this.nama = nama;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    @Override
    public String toString() {
        return "Semester{" +
                "kode='" + kode + '\'' +
                ", nama='" + nama + '\'' +
                '}';
    }
}
