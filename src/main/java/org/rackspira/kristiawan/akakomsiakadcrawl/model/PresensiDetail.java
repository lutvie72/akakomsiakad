package org.rackspira.kristiawan.akakomsiakadcrawl.model;

/**
 * Created by kal on 6/27/2017.
 */
public class PresensiDetail {
    private int no;
    private MataKuliah mataKuliah;
    private String kelas;
    private String jumlahKuliah;
    private String hadir;
    private String ijin;
    private String sakit;
    private String alpa;
    private String hadirPersentase;

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public MataKuliah getMataKuliah() {
        return mataKuliah;
    }

    public void setMataKuliah(MataKuliah mataKuliah) {
        this.mataKuliah = mataKuliah;
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public String getJumlahKuliah() {
        return jumlahKuliah;
    }

    public void setJumlahKuliah(String jumlahKuliah) {
        this.jumlahKuliah = jumlahKuliah;
    }

    public String getHadir() {
        return hadir;
    }

    public void setHadir(String hadir) {
        this.hadir = hadir;
    }

    public String getIjin() {
        return ijin;
    }

    public void setIjin(String ijin) {
        this.ijin = ijin;
    }

    public String getSakit() {
        return sakit;
    }

    public void setSakit(String sakit) {
        this.sakit = sakit;
    }

    public String getAlpa() {
        return alpa;
    }

    public void setAlpa(String alpa) {
        this.alpa = alpa;
    }

    public String getHadirPersentase() {
        return hadirPersentase;
    }

    public void setHadirPersentase(String hadirPersentase) {
        this.hadirPersentase = hadirPersentase;
    }

    @Override
    public String toString() {
        return "Presensi{" +
                "no=" + no +
                ", mataKuliah=" + mataKuliah +
                ", kelas='" + kelas + '\'' +
                ", jumlahKuliah='" + jumlahKuliah + '\'' +
                ", hadir='" + hadir + '\'' +
                ", ijin='" + ijin + '\'' +
                ", sakit='" + sakit + '\'' +
                ", alpa='" + alpa + '\'' +
                ", hadirPersentase='" + hadirPersentase + '\'' +
                '}';
    }
}
