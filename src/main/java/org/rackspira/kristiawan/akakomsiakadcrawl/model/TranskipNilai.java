package org.rackspira.kristiawan.akakomsiakadcrawl.model;

import java.util.List;

/**
 * Created by kal on 6/28/2017.
 */
public class TranskipNilai {

    private String judul;
    private User user;
    private List<TranskipNilaiDetail> transkipNilaiDetails;
    private String jumlahSksDiambil;
    private String jumlahMatakuliahDiambil;
    private String ipKomulatif;

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<TranskipNilaiDetail> getTranskipNilaiDetails() {
        return transkipNilaiDetails;
    }

    public void setTranskipNilaiDetails(List<TranskipNilaiDetail> transkipNilaiDetails) {
        this.transkipNilaiDetails = transkipNilaiDetails;
    }

    public String getJumlahSksDiambil() {
        return jumlahSksDiambil;
    }

    public void setJumlahSksDiambil(String jumlahSksDiambil) {
        this.jumlahSksDiambil = jumlahSksDiambil;
    }

    public String getJumlahMatakuliahDiambil() {
        return jumlahMatakuliahDiambil;
    }

    public void setJumlahMatakuliahDiambil(String jumlahMatakuliahDiambil) {
        this.jumlahMatakuliahDiambil = jumlahMatakuliahDiambil;
    }

    public String getIpKomulatif() {
        return ipKomulatif;
    }

    public void setIpKomulatif(String ipKomulatif) {
        this.ipKomulatif = ipKomulatif;
    }

    @Override
    public String toString() {
        return "TranskipNilai{" +
                "judul='" + judul + '\'' +
                ", user=" + user +
                ", transkipNilaiDetails=" + transkipNilaiDetails +
                ", jumlahSksDiambil='" + jumlahSksDiambil + '\'' +
                ", jumlahMatakuliahDiambil='" + jumlahMatakuliahDiambil + '\'' +
                ", ipKomulatif='" + ipKomulatif + '\'' +
                '}';
    }
}
