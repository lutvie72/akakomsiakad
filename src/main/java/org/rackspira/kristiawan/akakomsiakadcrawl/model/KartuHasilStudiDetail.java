package org.rackspira.kristiawan.akakomsiakadcrawl.model;

/**
 * Created by kal on 6/28/2017.
 */
public class KartuHasilStudiDetail {

    private int no;
    private MataKuliah mataKuliah;
    private String kelas;
    private String wajibAtauPilihan;
    private String nilai;

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public MataKuliah getMataKuliah() {
        return mataKuliah;
    }

    public void setMataKuliah(MataKuliah mataKuliah) {
        this.mataKuliah = mataKuliah;
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public String getWajibAtauPilihan() {
        return wajibAtauPilihan;
    }

    public void setWajibAtauPilihan(String wajibAtauPilihan) {
        this.wajibAtauPilihan = wajibAtauPilihan;
    }

    public String getNilai() {
        return nilai;
    }

    public void setNilai(String nilai) {
        this.nilai = nilai;
    }

    @Override
    public String toString() {
        return "KartuHasilStudiDetail{" +
                "no=" + no +
                ", mataKuliah=" + mataKuliah +
                ", kelas='" + kelas + '\'' +
                ", wajibAtauPilihan='" + wajibAtauPilihan + '\'' +
                ", nilai='" + nilai + '\'' +
                '}';
    }
}
