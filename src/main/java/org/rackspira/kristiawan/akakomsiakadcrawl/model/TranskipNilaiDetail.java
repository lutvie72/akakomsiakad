package org.rackspira.kristiawan.akakomsiakadcrawl.model;

/**
 * Created by kal on 6/28/2017.
 */
public class TranskipNilaiDetail {

    private int no;
    private String semester;
    private MataKuliah mataKuliah;
    private String nilai;

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public MataKuliah getMataKuliah() {
        return mataKuliah;
    }

    public void setMataKuliah(MataKuliah mataKuliah) {
        this.mataKuliah = mataKuliah;
    }

    public String getNilai() {
        return nilai;
    }

    public void setNilai(String nilai) {
        this.nilai = nilai;
    }

    @Override
    public String toString() {
        return "TranskipNilaiDetail{" +
                "no=" + no +
                ", semester='" + semester + '\'' +
                ", mataKuliah=" + mataKuliah +
                ", nilai='" + nilai + '\'' +
                '}';
    }
}
