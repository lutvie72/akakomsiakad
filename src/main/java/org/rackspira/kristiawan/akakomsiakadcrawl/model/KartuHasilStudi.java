package org.rackspira.kristiawan.akakomsiakadcrawl.model;

import java.util.List;

/**
 * Created by kal on 6/28/2017.
 */
public class KartuHasilStudi {

    private String judul;
    private User user;
    private String semester;
    private List<KartuHasilStudiDetail> kartuHasilStudiDetails;
    private String jumlahSksDiambil;
    private String jumlahMatakuliahDiambil;
    private String ipSemester;

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public List<KartuHasilStudiDetail> getKartuHasilStudiDetails() {
        return kartuHasilStudiDetails;
    }

    public void setKartuHasilStudiDetails(List<KartuHasilStudiDetail> kartuHasilStudiDetails) {
        this.kartuHasilStudiDetails = kartuHasilStudiDetails;
    }

    public String getJumlahSksDiambil() {
        return jumlahSksDiambil;
    }

    public void setJumlahSksDiambil(String jumlahSksDiambil) {
        this.jumlahSksDiambil = jumlahSksDiambil;
    }

    public String getJumlahMatakuliahDiambil() {
        return jumlahMatakuliahDiambil;
    }

    public void setJumlahMatakuliahDiambil(String jumlahMatakuliahDiambil) {
        this.jumlahMatakuliahDiambil = jumlahMatakuliahDiambil;
    }

    public String getIpSemester() {
        return ipSemester;
    }

    public void setIpSemester(String ipSemester) {
        this.ipSemester = ipSemester;
    }

    @Override
    public String toString() {
        return "KartuHasilStudi{" +
                "judul='" + judul + '\'' +
                ", user=" + user +
                ", kartuRencanaStudis=" + kartuHasilStudiDetails +
                ", jumlahSksDiambil='" + jumlahSksDiambil + '\'' +
                ", jumlahMatakuliahDiambil='" + jumlahMatakuliahDiambil + '\'' +
                ", ipSemester='" + ipSemester + '\'' +
                '}';
    }
}
