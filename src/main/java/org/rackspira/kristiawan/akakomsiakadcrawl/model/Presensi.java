package org.rackspira.kristiawan.akakomsiakadcrawl.model;

import java.util.List;

/**
 * Created by kal on 6/26/2017.
 */
public class Presensi {

    private User user;
    private Semester semester;
    private List<PresensiDetail> presensiDetails;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Semester getSemester() {
        return semester;
    }

    public void setSemester(Semester semester) {
        this.semester = semester;
    }

    public List<PresensiDetail> getPresensiDetails() {
        return presensiDetails;
    }

    public void setPresensiDetails(List<PresensiDetail> presensiDetails) {
        this.presensiDetails = presensiDetails;
    }

    @Override
    public String toString() {
        return "Presensi{" +
                "user=" + user +
                ", presensiDetails=" + presensiDetails +
                '}';
    }
}
