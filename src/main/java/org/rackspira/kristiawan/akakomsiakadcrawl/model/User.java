package org.rackspira.kristiawan.akakomsiakadcrawl.model;

public class User {

	private String nim;
	private String password;
	private String nama;
	private String jurusan;
	private String dosenPA;
	private String session;

	public String getNim() {
		return nim;
	}

	public void setNim(String nim) {
		this.nim = nim;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getJurusan() {
		return jurusan;
	}

	public void setJurusan(String jurusan) {
		this.jurusan = jurusan;
	}

	public String getDosenPA() {
		return dosenPA;
	}

	public void setDosenPA(String dosenPA) {
		this.dosenPA = dosenPA;
	}

	public String getSession() {
		return session;
	}

	public void setSession(String session) {
		this.session = session;
	}

	@Override
	public String toString() {
		return "User{" +
				"nim='" + nim + '\'' +
				", password='" + password + '\'' +
				", nama='" + nama + '\'' +
				", jurusan='" + jurusan + '\'' +
				'}';
	}
}
