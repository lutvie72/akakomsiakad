package org.rackspira.kristiawan.akakomsiakadcrawl.model;

/**
 * Created by kal on 6/26/2017.
 */
public class KartuRencanaStudiJadwal {
    private int no;
    private int kelas;
    private MataKuliah mataKuliah;
    private int ke;
    private String ruang;
    private String hari;
    private String jam;

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public int getKelas() {
        return kelas;
    }

    public void setKelas(int kelas) {
        this.kelas = kelas;
    }

    public MataKuliah getMataKuliah() {
        return mataKuliah;
    }

    public void setMataKuliah(MataKuliah mataKuliah) {
        this.mataKuliah = mataKuliah;
    }

    public int getKe() {
        return ke;
    }

    public void setKe(int ke) {
        this.ke = ke;
    }

    public String getRuang() {
        return ruang;
    }

    public void setRuang(String ruang) {
        this.ruang = ruang;
    }

    public String getHari() {
        return hari;
    }

    public void setHari(String hari) {
        this.hari = hari;
    }

    public String getJam() {
        return jam;
    }

    public void setJam(String jam) {
        this.jam = jam;
    }

    @Override
    public String toString() {
        return "KartuRencanaStudiJadwal{" +
                "no=" + no +
                ", kelas=" + kelas +
                ", mataKuliah=" + mataKuliah +
                ", ke=" + ke +
                ", ruang='" + ruang + '\'' +
                ", hari='" + hari + '\'' +
                ", jam='" + jam + '\'' +
                '}';
    }
}
