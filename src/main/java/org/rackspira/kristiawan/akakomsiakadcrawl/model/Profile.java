package org.rackspira.kristiawan.akakomsiakadcrawl.model;

/**
 * Created by kal on 8/27/2017.
 */
public class Profile {

    private String nim;
    private String nama;
    private String alamat;
    private String ttl;
    private String agama;
    private String jenisKelamin;
    private String asalSLTA;
    private String tglTerdaftar;
    private String namaOrtuAyahIbu;
    private String alamatOrtu;
    private String wargaNegara;
    private String status;
    private String noTelpHp;

    public String getNim() {
        return nim;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getTtl() {
        return ttl;
    }

    public void setTtl(String ttl) {
        this.ttl = ttl;
    }

    public String getAgama() {
        return agama;
    }

    public void setAgama(String agama) {
        this.agama = agama;
    }

    public String getJenisKelamin() {
        return jenisKelamin;
    }

    public void setJenisKelamin(String jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }

    public String getAsalSLTA() {
        return asalSLTA;
    }

    public void setAsalSLTA(String asalSLTA) {
        this.asalSLTA = asalSLTA;
    }

    public String getTglTerdaftar() {
        return tglTerdaftar;
    }

    public void setTglTerdaftar(String tglTerdaftar) {
        this.tglTerdaftar = tglTerdaftar;
    }

    public String getNamaOrtuAyahIbu() {
        return namaOrtuAyahIbu;
    }

    public void setNamaOrtuAyahIbu(String namaOrtuAyahIbu) {
        this.namaOrtuAyahIbu = namaOrtuAyahIbu;
    }

    public String getAlamatOrtu() {
        return alamatOrtu;
    }

    public void setAlamatOrtu(String alamatOrtu) {
        this.alamatOrtu = alamatOrtu;
    }

    public String getWargaNegara() {
        return wargaNegara;
    }

    public void setWargaNegara(String wargaNegara) {
        this.wargaNegara = wargaNegara;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNoTelpHp() {
        return noTelpHp;
    }

    public void setNoTelpHp(String noTelpHp) {
        this.noTelpHp = noTelpHp;
    }
}
