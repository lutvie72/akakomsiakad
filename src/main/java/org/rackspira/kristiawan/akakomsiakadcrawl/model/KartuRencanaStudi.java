package org.rackspira.kristiawan.akakomsiakadcrawl.model;

import java.util.List;

/**
 * Created by kal on 6/26/2017.
 */
public class KartuRencanaStudi {

    private String judul;
    private String semester;
    private User user;
    private List<KartuRencanaStudiJadwal> kartuRencanaStudiJadwals;
    private String ipSemesterLalu;
    private int jumlahKredit;
    private String maksimalSks;

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<KartuRencanaStudiJadwal> getKartuRencanaStudiJadwals() {
        return kartuRencanaStudiJadwals;
    }

    public void setKartuRencanaStudiJadwals(List<KartuRencanaStudiJadwal> kartuRencanaStudiJadwals) {
        this.kartuRencanaStudiJadwals = kartuRencanaStudiJadwals;
    }

    public String getIpSemesterLalu() {
        return ipSemesterLalu;
    }

    public void setIpSemesterLalu(String ipSemesterLalu) {
        this.ipSemesterLalu = ipSemesterLalu;
    }

    public int getJumlahKredit() {
        return jumlahKredit;
    }

    public void setJumlahKredit(int jumlahKredit) {
        this.jumlahKredit = jumlahKredit;
    }

    public String getMaksimalSks() {
        return maksimalSks;
    }

    public void setMaksimalSks(String maksimalSks) {
        this.maksimalSks = maksimalSks;
    }

    @Override
    public String toString() {
        return "KartuRencanaStudi{" +
                "judul='" + judul + '\'' +
                ", semester='" + semester + '\'' +
                ", user=" + user +
                ", kartuRencanaStudiJadwals=" + kartuRencanaStudiJadwals +
                ", ipSemesterLalu='" + ipSemesterLalu + '\'' +
                ", jumlahKredit=" + jumlahKredit +
                ", maksimalSks='" + maksimalSks + '\'' +
                '}';
    }
}
