package org.rackspira.kristiawan.akakomsiakadcrawl.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "org.rackspira.kristiawan.akakomsiakadcrawl")
public class WebConfig {
	

}